# `deus` - A `sudo` alternative that does not require SUID, nor is associated with systemd
## Why?
### I do not like systemd.
systemd, while being a fairly well modulated project, as in, it separates its components well,
it is still inherently very monolithic (it works with itself and only itself). 

It attempts, in one sole project, to replace many utilities with new ones that are not better than the previous ones.

And, if better than previous ones, they are usually unnecessarily heavy.
### I do not use systemd.
I value composition and individuality. Every project needs only implement the most simple of features. systemd is the opposite of what I value, so I don't use it.

I recommend openRC instead, or even dinit, but I haven't used the later.

### SUID is a bad idea.
Unix done goofed this. SUID binaries are binaries that always run as the owner, not the user that is running the binary.

This is fairly hacky, and can prove vulnerable.

### So...
To combat the EEE from systemd (embrace, extend, extinguish), I'm instead replacing one of their utils, `run0`, with my own implementation of the same mechanism. 

## The name
`deus`, in latin and portuguese, and probably other languages, means "god". This is to remind the user that when they use this command they obtain "godly" powers - and that comes with responsibility.

## Why Rust?
The programming language was chosen because this is a very security sensitive application. We will try to reduce the number of unsafe code used, but there is always some being used,
at least in the external libraries we depend on.
